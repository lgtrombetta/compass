    # window.py
#
# Simple GTK compass app for Mobile Linux
#
# Copyright 2021-2023 Leonardo G. Trombetta and Marco L. Trombetta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Handy, GLib, GdkPixbuf
#from gi_composites import GtkTemplate
from matplotlib.backends.backend_gtk3agg import (
    FigureCanvasGTK3Agg as FigureCanvas)
from matplotlib.figure import Figure
from threading import Thread

@Gtk.Template(resource_path='/com/gitlab/lgtrombetta/Compass/window.ui')
class CompassWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'CompassWindow'

    SettingsWindow = Gtk.Template.Child()
    dark_mode_switch = Gtk.Template.Child()
    currentcalib = Gtk.Template.Child()
    seconds = Gtk.Template.Child()
    calibratebtn = Gtk.Template.Child()
    calibresults = Gtk.Template.Child()
    savebtn = Gtk.Template.Child()
    clearbtn = Gtk.Template.Child()

    AboutWindow = Gtk.Template.Child()
    titlelabel = Gtk.Template.Child()
    logocanvas = Gtk.Template.Child()

    bigbox = Gtk.Template.Child()
    canvas = Gtk.Template.Child()
    toplabel1 = Gtk.Template.Child()
    toplabel2 = Gtk.Template.Child()
    toplabel3 = Gtk.Template.Child()
    bottomlabel = Gtk.Template.Child()


    def __init__(self, **kwargs):
        Handy.init()
        super().__init__(**kwargs)
        self.init_template()

        self.app = kwargs['application']

        self.settings = Gtk.Settings.get_default()
        self.settings.set_property("gtk-theme-name", "Adwaita")

        self.calibratedB0 = [0, 0, 0]

        path = '/com/gitlab/lgtrombetta/Compass/compass.svg'
        width = -1
        height = 120
        preserve_aspect_ratio = True

        logo = GdkPixbuf.Pixbuf.new_from_resource_at_scale(path, width, height, preserve_aspect_ratio)
        self.logocanvas.set_from_pixbuf(logo)

    @Gtk.Template.Callback()
    def on_settings_activate(self, *args):
        self.currentcalib.set_markup("B0[raw] = %s" % str(self.app.cmp.B0))
        self.SettingsWindow.show_all()

    @Gtk.Template.Callback()
    def on_dark_mode_switch_state_set(self, *args):
        self.app.setDarkTheme(not self.app.THEME == "dark")

    @Gtk.Template.Callback()
    def on_dark_mode_switch_activate(self, *args):
        print("test")

    @Gtk.Template.Callback()
    def on_calibrate_clicked(self, *args):
        if self.app.cmp.calib_countdown == 0:
            self.seconds.set_sensitive(False)
            self.calibresults.set_label("\nRotate your device\naround the 3 axes...\n\n")
            self.savebtn.set_sensitive(False)
            self.clearbtn.set_sensitive(False)

            duration = int(self.seconds.get_value())

            if duration <= 0:
                duration = 30

            self.calibratebtn.set_label("%d" % duration)

            self.t1 = Thread(target=self.app.cmp.calibrationRun, args = (self.on_calibration_end, duration))
            self.t1.start()
        else:
            self.app.cmp.calib_countdown = 0

    def on_calibration_end(self, meanB, elapsed):
        self.calibratedB0 = meanB

        GLib.idle_add(self.set_buttons_after_calib)

        res = "<Bx> + B0x [raw] = %d\n<By> + B0y [raw] = %d\n<Bz> + B0z [raw] = %d\n\nElapsed time: %0.2f seconds" % (int(meanB[0]), int(meanB[1]), int(meanB[2]), elapsed)
        GLib.idle_add(self.calibresults.set_label, res)

    def set_buttons_after_calib(self):
        self.calibratebtn.set_label("Calibrate")
        self.seconds.set_sensitive(True)
        self.savebtn.set_sensitive(True)
        self.clearbtn.set_sensitive(True)

    @Gtk.Template.Callback()
    def on_savebtn_clicked(self, *args):
        self.app.save_config(section="device", key="MAGN_X_OFFSET", value=str(self.calibratedB0[0]))
        self.app.save_config(section="device", key="MAGN_Y_OFFSET", value=str(self.calibratedB0[1]))
        self.app.save_config(section="device", key="MAGN_Z_OFFSET", value=str(self.calibratedB0[2]), reload=True)

        self.app.cmp.setOffset(offset=self.calibratedB0)
        self.currentcalib.set_markup("B0[raw] = %s" % str(self.app.cmp.B0))
        self.app.update_label('Calibration data loaded', color='green', pos=1)

        self.savebtn.set_sensitive(False)
        self.clearbtn.set_sensitive(False)
        self.calibresults.set_label("\n\n\n\n")

    @Gtk.Template.Callback()
    def on_clearbtn_clicked(self, *args):
        self.calibratedB0 = [0, 0, 0]

        self.savebtn.set_sensitive(False)
        self.clearbtn.set_sensitive(False)
        self.calibresults.set_label("\n\n\n\n")

    @Gtk.Template.Callback()
    def on_about_activate(self, *args):
        self.titlelabel.set_markup("<big><b>Compass v%s</b></big>" % self.app.version)
        self.AboutWindow.show_all()

    @Gtk.Template.Callback()
    def on_back_from_settings_clicked(self, *args):
        self.SettingsWindow.hide()

    @Gtk.Template.Callback()
    def on_back_from_about_clicked(self, *args):
        self.AboutWindow.hide()
