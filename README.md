# Compass <img src="data/icons/hicolor/scalable/apps/com.gitlab.lgtrombetta.Compass.svg" alt="Compass" title="" width="40" height="40" />

(formerly pinephone-compass)

This is a simple GTK3 compass app for Mobile Linux. 

---

Currently supported devices:

* Pine64 Pinephone v1.0, v1.1, v1.2 (LIS3MDL magnetometer)
* Pine64 Pinephone v1.2b (AF8133J magnetometer)
* Pine64 Pinephone Pro (AF8133J magnetometer, requires Megi's kernel>=6.3 for the correct [mount-matrix](https://github.com/megous/linux/commit/cf46f43b097bbe5d7a5f96c7c490eae9dee7390b))
* Purism Librem 5 (LSM9DS1 magnetometer)

Known issues:

* AF8133J is not currently recognized in [pmOS](https://gitlab.com/postmarketOS/pmaports/-/issues/1945)

---

<img src="data/screenshot.png" alt="Pinephone Compass" title="" width=20% height=auto />

---

### Dependencies

The app uses meson build system. Some packages are needed:

Manjaro:
```
sudo pacman -S meson pkgconf python-matplotlib python-numpy python-pandas
```
Mobian:
```
sudo apt install appstream-util gettext git libglib2.0-dev meson python3-matplotlib python3-numpy python3-pandas
```
postmarketOS:
```
sudo apk add desktop-file-utils gettext git glib-dev meson py3-matplotlib py3-numpy py3-pandas py3-xdg
```

The Python modules above can alternatively be installed via `pip`

### Installation

```
git clone https://gitlab.com/lgtrombetta/compass.git
cd compass
meson setup build
cd build
sudo ninja install
```

### Running the app

You can simply start the app from its launcher: <img src="data/icons/hicolor/scalable/apps/com.gitlab.lgtrombetta.Compass.svg" alt="Compass" title="" width="25" height="25" />

Alternatively, you can run the app from the terminal with `compass`

### Magnetometer Calibration

The reading of the magnetometer provided by the driver needs to be corrected to compensate for effects due to the hardware specifics of the device. The most important effect is a hardiron constant offset. Currently, the app performs this correction by sustracting the values `MAGN_X_OFFSET`, `MAGN_Y_OFFSET` and `MAGN_Z_OFFSET` which are defined in the configuration file `compass.conf`. These values can vary from device to device, and therefore it is necessary to perform a calibration run on each device at least once. You can do so by going to `Settings > Calibrate`.

The default duration will be `30` seconds. During this time, gently rotate your device around each of the 3 main axes repeatedly (try to do it on the same spot such that the total field intensity remains roughly constant). 

If the compass does not behave correctly for you, consider running the calibration again with a longer duration. Also, consider doing it on place away from external magnetic interference.

---

<div>Icons made by <a href="https://www.flaticon.com/authors/vectors-market" title="Vectors Market">Vectors Market</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
